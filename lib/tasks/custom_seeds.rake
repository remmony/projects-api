# frozen_string_literal: true

namespace :custom_seeds do
  desc 'Generate Users'

  task all: :environment do
    Rake::Task['custom_seeds:seed_test_user'].invoke
    Rake::Task['custom_seeds:seed_managers'].invoke
    Rake::Task['custom_seeds:seed_participants'].invoke
    Rake::Task['custom_seeds:seed_projects'].invoke
  end
  task seed_test_user: :environment do
    puts 'Creating Test User '

    Employee.create!(email: 'admin@example.com', full_name: Faker::Name.name_with_middle,
                     role: 'manager',
                     department: 'Sales',
                     authentication_token: '123456',
                     password: '123456', password_confirmation: '123456')
  end
  task seed_managers: :environment do
    departments = Settings.departments
    puts 'Creating managers'
    departments.each do |department|
      10.times do
        Employee.create!(email: "#{Random.hex(6)}_example@example.com", full_name: Faker::Name.name_with_middle,
                         role: 'manager',
                         department:,
                         password: '123456', password_confirmation: '123456')
      end
    end
  end
  task seed_participants: :environment do
    puts 'Creating participants'
    departments = Settings.departments
    departments.each do |department|
      10.times do
        Employee.create!(email: "#{Random.hex(6)}_example2@example.com", full_name: Faker::Name.name_with_middle,
                         role: 'participant',
                         department:,
                         password: '123456', password_confirmation: '123456')
      end
    end
  end

  task seed_projects: :environment do
    puts 'Creating projects'
    100.times do
      owner = Employee.manager.sample
      Project.create!(name: "#{Faker::FunnyName.name} #{Random.hex(4)}",
                      state: rand(0..3),
                      progress: rand(0..100),
                      owner:)
    end
    50.times do
      owner = Employee.manager.sample
      assignee = Employee.participant.sample
      Project.create!(name: "#{Faker::FunnyName.name} #{Random.hex(4)}",
                      state: rand(0..3),
                      progress: rand(0..100),
                      assignee:,
                      owner:)
    end
  end
end
