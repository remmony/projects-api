FROM ruby:3.1.3-bullseye

RUN apt-get update -qq && apt-get install -y curl postgresql-client ca-certificates


RUN mkdir -p /myapp

# Create custom user
RUN useradd --create-home --shell /bin/bash myrails_user \
    && passwd --delete myrails_user \
    && adduser myrails_user sudo
WORKDIR /myapp

COPY Gemfile Gemfile.lock ./
RUN gem install bundler
RUN bundle install
COPY . .


EXPOSE 3000
RUN chown -R myrails_user:myrails_user /myapp
RUN chmod 775 /myapp
USER myrails_user
CMD ["rails", "server", "-b", "0.0.0.0"]
