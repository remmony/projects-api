# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id          :uuid             not null, primary key
#  name        :string           not null
#  progress    :float            default(0.0), not null
#  state       :integer          default("planned"), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  assignee_id :uuid
#  owner_id    :uuid             not null
#
# Indexes
#
#  index_projects_on_assignee_id  (assignee_id)
#  index_projects_on_owner_id     (owner_id)
#
# Foreign Keys
#
#  fk_rails_...  (assignee_id => employees.id)
#  fk_rails_...  (owner_id => employees.id)
#
FactoryBot.define do
  factory :project do
    sequence(:name) { |n| "#{Faker::FunnyName.name} #{n}" }
    owner { create(:employee, :manager) }
    assignee { create(:employee) }
    state { Settings.projects.states.sample }
    progress { rand(0..100) }
  end
end
