# frozen_string_literal: true

# == Schema Information
#
# Table name: employees
#
#  id                     :uuid             not null, primary key
#  authentication_token   :string
#  department             :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  full_name              :string           not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :integer          default("participant"), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_employees_on_authentication_token  (authentication_token) UNIQUE
#  index_employees_on_department            (department)
#  index_employees_on_email                 (email) UNIQUE
#  index_employees_on_reset_password_token  (reset_password_token) UNIQUE
#
FactoryBot.define do
  factory :employee do
    full_name { Faker::Name.unique.name }
    email { Faker::Internet.unique.email }
    password { '12345678' }
    password_confirmation { '12345678' }
    department { Settings.departments.sample }
    trait :manager do
      role { 'manager' }
    end
  end
end
