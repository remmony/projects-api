# == Schema Information
#
# Table name: projects
#
#  id          :uuid             not null, primary key
#  name        :string           not null
#  progress    :float            default(0.0), not null
#  state       :integer          default("planned"), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  assignee_id :uuid
#  owner_id    :uuid             not null
#
# Indexes
#
#  index_projects_on_assignee_id  (assignee_id)
#  index_projects_on_owner_id     (owner_id)
#
# Foreign Keys
#
#  fk_rails_...  (assignee_id => employees.id)
#  fk_rails_...  (owner_id => employees.id)
#
require 'rails_helper'

RSpec.describe Project, type: :model do
  subject do
    create(:project, owner: create(:employee, :manager))
  end

  it { is_expected.to validate_uniqueness_of(:name) }
  it { is_expected.to belong_to(:owner).class_name('Employee') }
  it { is_expected.to belong_to(:assignee).class_name('Employee').optional }

  it {
    expect(subject).to validate_inclusion_of(:progress).in_range(0..100)
                                                       .with_message('Invalid Progress Value')
  }
end
