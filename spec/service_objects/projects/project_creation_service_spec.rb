# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Projects::ProjectCreationService, type: :helpers do
  subject(:service) { described_class.new(params, employee) }

  let(:employee) { create(:employee, :manager) }
  let(:name) { 'name1' }
  let(:params) do
    { name: }
  end

  describe '#perform' do
    subject(:perform) { service.perform }

    context 'when there is an error ccreating the project' do
      before do
        create(:project, name:)
      end

      it 'raises an error' do
        expect { perform }.to raise_error(Errors::ProjectCreationError)
      end
    end

    it 'creates the project' do
      perform

      project = Project.last

      expect(project.name).to eq(name)
      expect(project.owner_id).to eq(employee.id)
      expect(project.state).to eq('planned')
      expect(project.progress).to eq(0)
    end
  end
end
