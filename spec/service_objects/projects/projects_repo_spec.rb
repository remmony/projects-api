# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Projects::ProjectsRepo, type: :helpers do
  subject(:repo) { described_class.new(params) }

  let(:params) do
    {}
  end

  let(:num) { 10 }
  let!(:projects) do
    create_list(:project, num)
  end
  let(:total_pages) { (Float(Project.count) / 25).ceil }
  let(:next_page) do
    2 if total_pages > 1
  end
  let(:sort_by) { 'created_at' }
  let(:sort_order) { 'asc' }
  let(:expected_meta_data) do
    { total_pages:,
      current_page: 1,
      next_page:,
      prev_page: nil,
      sort_by:,
      sort_order:,
      per_page: 25 }
  end
  let(:expected_projects) do
    Project.order(created_at: :asc).page(1).per(25)
  end
  let(:expected_data) do
    OpenStruct.new({ data: expected_projects,
                     meta: expected_meta_data })
  end

  describe '#list_projects' do
    subject(:list_projects) { repo.list_projects }

    context 'when sorting by created_at desc' do
      let(:sort_order) { 'desc' }
      let(:params) do
        { sort_order: }
      end
      let(:expected_projects) do
        Project.order(created_at: :desc).page(1).per(25)
      end

      it 'returns list of projects sorted by name' do
        expect(list_projects).to eq(expected_data)
      end
    end

    context 'when sorting by name desc' do
      let(:sort_by) { 'name' }
      let(:sort_order) { 'desc' }
      let(:params) do
        { sort_by:, sort_order: }
      end
      let(:expected_projects) do
        Project.order(name: :desc).page(1).per(25)
      end

      it 'returns list of projects sorted by name desc' do
        expect(list_projects).to eq(expected_data)
      end
    end

    context 'when sorting by name asc' do
      let(:sort_by) { 'name' }
      let(:params) do
        { sort_by: }
      end
      let(:expected_projects) do
        Project.order(name: :asc).page(1).per(25)
      end

      it 'returns list of projects sorted by name asc' do
        expect(list_projects).to eq(expected_data)
      end
    end

    context 'when filtering by state done' do
      let(:total_pages) { (Float(Project.done.count) / 25).ceil }
      let(:state) { 'done' }
      let(:params) do
        { state: }
      end
      let(:expected_projects) do
        Project
          .done
          .order(created_at: :asc).page(1).per(25)
      end

      it 'returns list of done projects sorted by created at asc' do
        expect(list_projects).to eq(expected_data)
      end
    end

    context 'when filtering by state failed' do
      let(:total_pages) { (Float(Project.failed.count) / 25).ceil }
      let(:state) { 'failed' }
      let(:params) do
        { state: }
      end
      let(:expected_projects) do
        Project
          .failed
          .order(created_at: :asc).page(1).per(25)
      end

      it 'returns list of failed projects sorted by created at asc' do
        expect(list_projects).to eq(expected_data)
      end
    end

    context 'when filtering by state planned' do
      let(:total_pages) { (Float(Project.planned.count) / 25).ceil }
      let(:state) { 'planned' }
      let(:params) do
        { state: }
      end
      let(:expected_projects) do
        Project
          .planned
          .order(created_at: :asc).page(1).per(25)
      end

      it 'returns list of planned projects sorted by created at asc' do
        expect(list_projects).to eq(expected_data)
      end
    end

    context 'when filtering by state active' do
      let(:total_pages) { (Float(Project.active.count) / 25).ceil }
      let(:state) { 'active' }
      let(:params) do
        { state: }
      end
      let(:expected_projects) do
        Project
          .active
          .order(created_at: :asc).page(1).per(25)
      end

      it 'returns list of active projects sorted by created at asc' do
        expect(list_projects).to eq(expected_data)
      end
    end

    it 'returns list of projects sorted by created at asc' do
      expect(list_projects).to eq(expected_data)
    end
  end
end
