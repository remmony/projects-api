# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Projects::ProjectUpdateService, type: :helpers do
  subject(:service) { described_class.new(params, project) }

  let(:employee) { create(:employee, :manager) }
  let(:name) { 'name1' }
  let(:state) { 'planned' }
  let(:progress) { 0 }
  let!(:project) do
    create(:project, name:, state:, progress:, assignee: nil, owner: employee)
  end
  let(:updated_name) { 'name2' }
  let(:updated_state) { 'active' }
  let(:updated_progress) { 80 }

  let(:assignee) do
    create(:employee, department: employee.department)
  end
  let(:params) do
    { name: updated_name, state: updated_state, progress: updated_progress, assignee_id: assignee.id }
  end

  describe '#perform' do
    subject(:perform) { service.perform }

    context 'when new assignee is not from the same owner department' do
      let(:assignee) do
        create(:employee, department: 'not_department')
      end

      it 'raises an error' do
        expect { perform }.to raise_error(Errors::AssigneeNotInDepartmentError)
      end
    end

    context 'when there is a project with the same name' do
      let(:updated_name) { 'new_name' }

      before do
        create(:project, name: updated_name)
      end

      it 'raises an error' do
        expect { perform }.to raise_error(Errors::ProjectUpdateError)
      end
    end

    it 'updates the project' do
      expect { perform }
        .to change(project, :name).from(name).to(updated_name)
        .and change(project, :state).from(state).to(updated_state)
        .and change(project, :progress).from(progress).to(updated_progress)
        .and change(project, :assignee_id).from(nil).to(assignee.id)
    end
  end
end
