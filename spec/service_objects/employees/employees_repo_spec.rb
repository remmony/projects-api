# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Employees::EmployeesRepo, type: :helpers do
  subject(:repo) { described_class.new(params) }

  let(:params) do
    {}
  end

  let(:num) { 10 }
  let!(:employees) do
    create_list(:employee, num)
  end
  let(:total_pages) { (Float(Employee.count) / 25).ceil }
  let(:next_page) do
    2 if total_pages > 1
  end
  let(:sort_by) { 'created_at' }
  let(:sort_order) { 'asc' }
  let(:expected_meta_data) do
    { total_pages:,
      current_page: 1,
      next_page:,
      prev_page: nil,
      sort_by:,
      sort_order:,
      per_page: 25 }
  end
  let(:expected_projects) do
    Employee.order(created_at: :asc).page(1).per(25)
  end
  let(:expected_data) do
    OpenStruct.new({ data: expected_projects,
                     meta: expected_meta_data })
  end

  describe '#list_employees' do
    subject(:list_employees) { repo.list_employees }

    context 'when sorting by created_at desc' do
      let(:sort_order) { 'desc' }
      let(:params) do
        { sort_order: }
      end
      let(:expected_projects) do
        Employee.order(created_at: :desc).page(1).per(25)
      end

      it 'returns list of employees sorted by name' do
        expect(list_employees).to eq(expected_data)
      end
    end

    context 'when sorting by name desc' do
      let(:sort_by) { 'name' }
      let(:sort_order) { 'desc' }
      let(:params) do
        { sort_by:, sort_order: }
      end
      let(:expected_projects) do
        Employee.order(name: :desc).page(1).per(25)
      end

      it 'returns list of employees sorted by name desc' do
        expect(list_employees).to eq(expected_data)
      end
    end

    context 'when sorting by name asc' do
      let(:sort_by) { 'name' }
      let(:params) do
        { sort_by: }
      end
      let(:expected_projects) do
        Employee.order(name: :asc).page(1).per(25)
      end

      it 'returns list of employees sorted by name asc' do
        expect(list_employees).to eq(expected_data)
      end
    end

    context 'when filtering by role manager' do
      let(:total_pages) { (Float(Employee.manager.count) / 25).ceil }
      let(:role) { 'manager' }
      let(:params) do
        { role: }
      end
      let(:expected_projects) do
        Employee
          .manager
          .order(created_at: :asc).page(1).per(25)
      end

      it 'returns list of manager employees sorted by created at asc' do
        expect(list_employees).to eq(expected_data)
      end
    end

    context 'when filtering by role participant' do
      let(:total_pages) { (Float(Employee.participant.count) / 25).ceil }
      let(:role) { 'participant' }
      let(:params) do
        { role: }
      end
      let(:expected_projects) do
        Employee
          .participant
          .order(created_at: :asc).page(1).per(25)
      end

      it 'returns list of participant employees sorted by created at asc' do
        expect(list_employees).to eq(expected_data)
      end
    end

    it 'returns list of employees sorted by created at asc' do
      expect(list_employees).to eq(expected_data)
    end
  end
end
