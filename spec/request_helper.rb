# frozen_string_literal: true

module RequestHelper
  def send_request(http_method, request_url, params, token)
    if %i[get delete].include?(http_method)
      send(http_method, request_url, params:, headers: {
             'CONTENT_TYPE' => 'application/json', 'Authorization' => token
           })

    else
      send http_method, request_url, params: params.to_json, headers: {
        'CONTENT_TYPE' => 'application/json', 'Authorization' => token
      }
    end
    response
  end
end
