# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::ProjectsApi, type: :request do
  def dispatch(http_method = :get)
    send_request(http_method, request_url, params, token)
  end
  let(:json_response) do
    ActiveSupport::JSON.decode response.body
  end
  let(:json_response_data) do
    json_response['data']
  end
  let(:token) { current_employee.authentication_token }
  let(:current_employee) { create(:employee, :manager) }
  let(:params) { {} }
  let(:total_pages) { (Float(Project.count) / 25).ceil }
  let(:next_page) do
    2 if total_pages > 1
  end
  let(:sort_by) { 'created_at' }
  let(:sort_order) { 'asc' }
  let(:expected_meta_data) do
    { total_pages:,
      current_page: 1,
      next_page:,
      prev_page: nil,
      sort_by:,
      sort_order:,
      per_page: 25 }.stringify_keys
  end

  describe 'Get api/v1.0/projects/' do
    subject(:dispatch_request) { dispatch }

    let(:request_url) { '/api/v1.0/projects/' }
    let(:num) { 10 }
    let!(:projects) do
      create_list(:project, num)
    end

    let(:expected_results) do
      Project.order(created_at: :asc)
    end

    context 'when tokens are invalid' do
      let(:token) { SecureRandom.hex(6) }

      it 'returns unauthenticated error' do
        expect(dispatch_request).to have_http_status :unauthorized
      end
    end

    context 'when there is a big list of projects' do
      let(:num) { 100 }
      let(:expected_results) do
        Project.order(created_at: :asc).first(25)
      end

      context 'when sort order is DESC' do
        let(:params) do
          { sort_order: }
        end
        let(:expected_results) do
          Project.order(created_at: :desc).first(25)
        end

        let(:sort_order) { 'desc' }

        it 'returns the list of projects order by created_at asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when sorting by full name asc' do
        let(:expected_results) do
          Project.order(name: :asc).first(25)
        end
        let(:params) do
          { sort_by: }
        end
        let(:sort_by) { 'name' }

        it 'returns the list of projects order by name asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when sorting by name desc' do
        let(:expected_results) do
          Project.order(name: :desc).first(25)
        end
        let(:params) do
          { sort_by:, sort_order: }
        end
        let(:sort_by) { 'name' }
        let(:sort_order) { 'desc' }

        it 'returns the list of projects order by name desc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when filtering by done state' do
        let(:params) do
          { state: 'done' }
        end
        let(:total_pages) { (Float(Project.done.count) / 25).ceil }

        let(:expected_results) do
          Project.done.order(created_at: :asc).first(25)
        end

        it 'returns the list of done projects order by created_at asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when filtering by planned state' do
        let(:params) do
          { state: 'planned' }
        end
        let(:total_pages) { (Float(Project.planned.count) / 25).ceil }

        let(:expected_results) do
          Project.planned.order(created_at: :asc).first(25)
        end

        it 'returns the list of planned projects order by created_at asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when filtering by failed state' do
        let(:params) do
          { state: 'failed' }
        end
        let(:total_pages) { (Float(Project.failed.count) / 25).ceil }

        let(:expected_results) do
          Project.failed.order(created_at: :asc).first(25)
        end

        it 'returns the list of failed projects order by created_at asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when filtering by active state' do
        let(:params) do
          { state: 'active' }
        end
        let(:total_pages) { (Float(Project.active.count) / 25).ceil }

        let(:expected_results) do
          Project.active.order(created_at: :asc).first(25)
        end

        it 'returns the list of active projects order by created_at asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      it 'returns the list of projects order by created_at asc' do
        expect(dispatch_request).to have_http_status :ok
        expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
        expect(json_response['meta']).to eq(expected_meta_data)
      end
    end

    it 'returns the list of projects order by created_at asc' do
      expect(dispatch_request).to have_http_status :ok
      expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
      expect(json_response['meta']).to eq(expected_meta_data)
    end
  end

  describe 'PATCH api/v1.0/projects/' do
    subject(:dispatch_request) { dispatch(:patch) }

    let(:name) { 'name1' }
    let(:state) { 'planned' }
    let(:progress) { 0 }
    let!(:project) do
      create(:project, name:, state:, progress:, assignee: nil, owner: current_employee)
    end

    let(:updated_name) { 'name2' }
    let(:updated_state) { 'active' }
    let(:updated_progress) { 80 }

    let(:assignee) do
      create(:employee, department: current_employee.department)
    end
    let(:params) do
      { name: updated_name, state: updated_state, progress: updated_progress, assignee_id: assignee.id }
    end

    let(:request_url) { "/api/v1.0/projects/#{project.id}" }

    let(:owner_data) do
      { full_name: current_employee.full_name,
        id: current_employee.id,
        resource_type: 'Employee' }
    end
    let(:assignee_data) do
      { full_name: assignee.full_name,
        id: assignee.id,
        resource_type: 'Employee' }
    end

    context 'when tokens are invalid' do
      let(:token) { SecureRandom.hex(6) }

      it 'returns unauthenticated error' do
        expect(dispatch_request).to have_http_status :unauthorized
      end
    end

    context 'when the current employee is not a manager' do
      let(:current_employee) { create(:employee) }

      it 'returns forbidden status' do
        expect(dispatch_request).to have_http_status :forbidden
      end
    end

    context 'when there is a project with the same name' do
      let(:updated_name) { 'new_name' }

      before do
        create(:project, name: updated_name)
      end

      it 'returns unprocessable_entity status' do
        expect(dispatch_request).to have_http_status :unprocessable_entity
        expect(json_response['error']).to include('Validation failed: Name has already been taken')
      end
    end

    it 'updates the project' do
      expect(dispatch_request).to have_http_status :ok
      expect(json_response_data['resource_type']).to eq('Project')
      expect(json_response_data['name']).to eq(updated_name)
      expect(json_response_data['state']).to eq(updated_state)
      expect(json_response_data['progress']).to eq(updated_progress)
      expect(json_response_data['assignee']).to eq(assignee_data.stringify_keys)
      expect(json_response_data['owner']).to eq(owner_data.stringify_keys)
    end
  end

  describe 'POST api/v1.0/projects/' do
    subject(:dispatch_request) { dispatch(:post) }

    let(:name) { 'project_name' }
    let(:date_time) { DateTime.now }
    let(:params) do
      {
        name:
      }
    end

    let(:request_url) { '/api/v1.0/projects/' }

    let(:owner_data) do
      { full_name: current_employee.full_name,
        id: current_employee.id,
        resource_type: 'Employee' }
    end

    let(:time) { Time.now }

    before do
      allow(Time).to receive(:now).and_return(time)
    end

    context 'when tokens are invalid' do
      let(:token) { SecureRandom.hex(6) }

      it 'returns unauthenticated error' do
        expect(dispatch_request).to have_http_status :unauthorized
      end
    end

    context 'when the current employee is not a manager' do
      let(:current_employee) { create(:employee) }

      it 'returns forbidden status' do
        expect(dispatch_request).to have_http_status :forbidden
      end
    end

    context 'when there is a project with the same name' do
      before do
        create(:project, name:)
      end

      it 'returns unprocessable_entity status' do
        expect(dispatch_request).to have_http_status :unprocessable_entity
        expect(json_response['error']).to include('Validation failed: Name has already been taken')
      end
    end

    it 'creates the project' do
      expect(dispatch_request).to have_http_status :created
      expect(json_response_data['resource_type']).to eq('Project')
      expect(json_response_data['name']).to eq(name)
      expect(json_response_data['state']).to eq('planned')
      expect(json_response_data['progress']).to eq(0)
      expect(json_response_data['assignee']).to be_nil
      expect(json_response_data['created_at']).to eq(time.iso8601)
      expect(json_response_data['owner']).to eq(owner_data.stringify_keys)
    end
  end

  describe 'Get api/v1.0/projects/{id}' do
    subject(:dispatch_request) { dispatch }

    let(:project) { create(:project, assignee: nil) }

    let(:request_url) { "/api/v1.0/projects/#{project.id}" }

    let(:owner_data) do
      { full_name: project.owner.full_name,
        id: project.owner_id,
        resource_type: 'Employee' }
    end

    context 'when tokens are invalid' do
      let(:token) { SecureRandom.hex(6) }

      it 'returns unauthenticated error' do
        expect(dispatch_request).to have_http_status :unauthorized
      end
    end

    it 'returns the project data' do
      expect(dispatch_request).to have_http_status :ok
      expect(json_response_data['resource_type']).to eq('Project')
      expect(json_response_data['name']).to eq(project.name)
      expect(json_response_data['state']).to eq(project.state)
      expect(json_response_data['progress']).to eq(project.progress)
      expect(json_response_data['assignee']).to be_nil
      expect(json_response_data['created_at']).to eq(project.created_at.iso8601)
      expect(json_response_data['owner']).to eq(owner_data.stringify_keys)
    end
  end
end
