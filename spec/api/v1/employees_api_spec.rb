# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::EmployeesApi, type: :request do
  def dispatch(http_method = :get)
    send_request(http_method, request_url, params, token)
  end
  let(:json_response) do
    ActiveSupport::JSON.decode response.body
  end
  let(:json_response_data) do
    json_response['data']
  end
  let(:token) { current_employee.authentication_token }
  let(:current_employee) { create(:employee, :manager) }
  let(:params) { {} }
  let(:total_pages) { (Float(Employee.count) / 25).ceil }
  let(:next_page) do
    2 if total_pages > 1
  end
  let(:expected_meta_data) do
    { total_pages:,
      current_page: 1,
      next_page:,
      prev_page: nil,
      sort_by:,
      sort_order:,
      per_page: 25 }.stringify_keys
  end
  let(:sort_by) { 'created_at' }
  let(:sort_order) { 'asc' }

  describe 'Get api/v1.0/employees/' do
    subject(:dispatch_request) { dispatch }

    let(:request_url) { '/api/v1.0/employees/' }
    let(:num) { 10 }
    let!(:employees) do
      create_list(:employee, num)
    end

    let(:expected_results) do
      Employee.order(created_at: :asc)
    end

    context 'when tokens are invalid' do
      let(:token) { SecureRandom.hex(6) }

      it 'returns unauthenticated error' do
        expect(dispatch_request).to have_http_status :unauthorized
      end
    end

    context 'when there is a big list of employees' do
      let(:num) { 100 }
      let(:expected_results) do
        Employee.order(created_at: :asc).first(25)
      end

      context 'when sort order is DESC' do
        let(:params) do
          { sort_order: }
        end
        let(:expected_results) do
          Employee.order(created_at: :desc).first(25)
        end

        let(:sort_order) { 'desc' }

        it 'returns the list of employees order by created_at asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when sorting by full name asc' do
        let(:expected_results) do
          Employee.order(full_name: :asc).first(25)
        end
        let(:params) do
          { sort_by: }
        end
        let(:sort_by) { 'full_name' }

        it 'returns the list of employees order by full_name asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when sorting by role asc' do
        let(:expected_results) do
          Employee.order(role: :asc).first(25)
        end
        let(:params) do
          { sort_by: }
        end
        let(:sort_by) { 'role' }

        it 'returns the list of employees order by role asc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when sorting by role desc' do
        let(:expected_results) do
          Employee.order(role: :desc).first(25)
        end
        let(:params) do
          { sort_by:, sort_order: }
        end
        let(:sort_by) { 'role' }
        let(:sort_order) { 'desc' }

        it 'returns the list of employees order by role desc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      context 'when sorting by full name desc' do
        let(:expected_results) do
          Employee.order(full_name: :desc).first(25)
        end
        let(:params) do
          { sort_by:, sort_order: }
        end
        let(:sort_by) { 'full_name' }
        let(:sort_order) { 'desc' }

        it 'returns the list of employees order by full_name desc' do
          expect(dispatch_request).to have_http_status :ok
          expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
          expect(json_response['meta']).to eq(expected_meta_data)
        end
      end

      it 'returns the list of employees order by created_at asc' do
        expect(dispatch_request).to have_http_status :ok
        expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
        expect(json_response['meta']).to eq(expected_meta_data)
      end
    end

    context 'when filtering by manager role' do
      let(:params) do
        { role: 'manager' }
      end
      let!(:managers) do
        create_list(:employee, managers_count, :manager)
      end

      let(:managers_count) { 15 }
      let(:expected_results) do
        Employee.manager.order(created_at: :asc)
      end
      let(:total_pages) { (Float(Employee.manager.count) / 25).ceil }

      it 'returns the list of employees with the manager role' do
        expect(dispatch_request).to have_http_status :ok
        expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
        # returns list of new created managers + current_employee(manager)
        expect(json_response_data.count).to eq(managers_count + 1)
        expect(json_response['meta']).to eq(expected_meta_data)
      end
    end

    context 'when filtering by participants role' do
      let(:params) do
        { role: 'participant' }
      end

      let(:expected_results) do
        Employee.participant.order(created_at: :asc)
      end
      let(:total_pages) { (Float(Employee.participant.count) / 25).ceil }

      it 'returns the list of employees with the participant role' do
        expect(dispatch_request).to have_http_status :ok
        expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))

        expect(json_response['meta']).to eq(expected_meta_data)
      end
    end

    it 'returns the list of employees order by created_at asc' do
      expect(dispatch_request).to have_http_status :ok
      expect(json_response_data.map { |i| i['id'] }).to eq(expected_results.pluck(:id))
      expect(json_response['meta']).to eq(expected_meta_data)
    end
  end

  describe 'Get api/v1.0/employees/{id}' do
    subject(:dispatch_request) { dispatch }

    let(:employee) { create(:employee) }
    let(:request_url) { "/api/v1.0/employees/#{employee.id}" }

    context 'when tokens are invalid' do
      let(:token) { SecureRandom.hex(6) }

      it 'returns unauthenticated error' do
        expect(dispatch_request).to have_http_status :unauthorized
      end
    end

    it 'returns the employee data' do
      expect(dispatch_request).to have_http_status :ok
      expect(json_response_data['resource_type']).to eq('Employee')
      expect(json_response_data['full_name']).to eq(employee.full_name)
      expect(json_response_data['department']).to eq(employee.department)
      expect(json_response_data['role']).to eq(employee.role)
    end
  end

  describe 'Delete api/v1.0/employees/{id}' do
    subject(:dispatch_request) { dispatch(:delete) }

    let(:employee) { create(:employee) }
    let(:request_url) { "/api/v1.0/employees/#{employee.id}" }

    context 'when tokens are invalid' do
      let(:token) { SecureRandom.hex(6) }

      it 'returns unauthenticated error' do
        expect(dispatch_request).to have_http_status :unauthorized
        expect(json_response['error']).to eq('Unauthorized')
      end
    end

    context 'when the employee is a manager' do
      let(:employee) { create(:employee, :manager) }

      it 'returns forbidden status' do
        expect(dispatch_request).to have_http_status :forbidden
        expect(json_response['error']).to eq('Not allowed.')
      end
    end

    context 'when the current employee is not manager' do
      let(:current_employee) { create(:employee) }

      it 'returns forbidden status' do
        expect(dispatch_request).to have_http_status :forbidden
        expect(json_response['error']).to eq('Not allowed.')
      end
    end

    context 'when the employee has some assigned projects' do
      let!(:assigned_project) { create(:project, assignee: employee) }

      it 'returns unprocessable_entity status' do
        expect(dispatch_request).to have_http_status :unprocessable_entity
        expect(json_response['error']).to eq('Cannot delete record because of dependent assigned_projects')
      end
    end

    it 'deletes the employee' do
      expect(dispatch_request).to have_http_status :no_content
      expect(Employee.find_by_id(employee.id)).to be_nil
    end
  end
end
