# frozen_string_literal: true

module Helpers
  module AuthenticationHelper
    include ActionController::HttpAuthentication::Token

    def authenticate_user!
      error!({ error: 'Unauthorized' }, 401, 'Content-Type' => 'application/json') unless authenticated?
    end

    def authenticated?
      current_user.present?
    end

    def access_token
      token_params_from(headers['Authorization']).shift[1]
    end

    def current_user
      return nil if headers['Authorization'].nil?

      @current_user = Employee.find_by_authentication_token(access_token)
    end
  end
end
