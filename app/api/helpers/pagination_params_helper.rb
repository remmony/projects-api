# frozen_string_literal: true

module Helpers
  module PaginationParamsHelper
    extend Grape::API::Helpers

    params :pagination do |options|
      optional :sort_by, type: String,
                         desc: 'Sort By',
                         values: options[:sort_by_values]

      optional :sort_order, type: String,
                            desc: 'Sort Order', values: %w[desc asc]
      optional :page, type: Integer,
                      desc: 'Page param for pagination'
      optional :per_page, type: Integer,
                          desc: 'Per Page param for pagination'
    end
  end
end
