# frozen_string_literal: true

module ErrorsHandler
  extend ActiveSupport::Concern
  included do
    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error!({ error: "Invalid Api Request. #{e.message}" },
             422, 'Content-Type' => 'application/json')
    end

    rescue_from ActiveRecord::RecordNotFound do
      error!({ error: 'Resource Not found' }, 404, 'Content-Type' => 'application/json')
    end
    rescue_from ::Errors::ProjectCreationError do |e|
      error!({ error: "Project cannot be created . #{e.message}" },
             422, 'Content-Type' => 'application/json')
    end
    rescue_from ::Errors::ProjectUpdateError do |e|
      error!({ error: "Project cannot be updated . #{e.message}" },
             422, 'Content-Type' => 'application/json')
    end
    rescue_from ::Errors::InvalidAssigneeError do |_e|
      error!({ error: 'Assignee provided is not valid' },
             422, 'Content-Type' => 'application/json')
    end
    rescue_from ::Errors::AssigneeNotInDepartmentError do |_e|
      error!({ error: 'Assignee is not from the same project owner department' },
             422, 'Content-Type' => 'application/json')
    end

    rescue_from ActiveRecord::DeleteRestrictionError do |e|
      error!({ error: e.to_s },
             422, 'Content-Type' => 'application/json')
    end
    rescue_from ActiveRecord::NotNullViolation do |e|
      error!({ error: "Resource cannot be created . #{e.message}" },
             422, 'Content-Type' => 'application/json')
    end

    rescue_from Pundit::NotAuthorizedError do
      error!({ error: 'Not allowed.' }, 403, 'Content-Type' => 'application/json')
    end
    rescue_from :all do |_|
      error!({ error: 'Something wrong happened' }, 500, 'Content-Type' => 'application/json')
    end
  end
end
