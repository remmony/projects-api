# frozen_string_literal: true

module Api
  class V1Base < Grape::API
    version 'v1.0', using: :path
    mount Api::V1::EmployeesApi
    mount Api::V1::ProjectsApi
  end
end
