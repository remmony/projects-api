# frozen_string_literal: true

module Api
  module V1
    class ProjectsApi < Grape::API
      helpers ::Helpers::PaginationParamsHelper
      before do
        authenticate_user!
      end
      helpers do
        def resource_params
          # include_missing (false) to remove records with nil values
          @resource_params ||= declared(params, include_missing: false)
        end

        def projects_repo
          @projects_repo ||= ::Projects::ProjectsRepo.new(resource_params)
        end
      end
      namespace :projects do
        desc 'List all projects', success: ::Entities::V1::Project

        params do
          use :pagination,
              sort_by_values: %w[created_at name], default_sort_by_value: 'created_at'
          optional :state, type: String,
                           desc: 'Filter by project state',
                           values: Settings.projects.states,
                           allow_blank: true
        end
        get '/' do
          authorize Project, :index?
          results = projects_repo.list_projects
          present :data, results.data, with: ::Entities::V1::Project
          present :meta, results.meta, with: ::Entities::V1::MetaData
        end

        desc 'Create a new project', success: ::Entities::V1::Project
        helpers do
          def project_creation_service
            @project_creation_service ||= Projects::ProjectCreationService.new(resource_params, current_user)
          end
        end
        params do
          requires :name, type: String,
                          desc: 'Project Name',
                          documentation: { param_type: 'body' },
                          allow_blank: false
        end
        post '/' do
          authorize Project, :create?
          project = project_creation_service.perform
          present :data, project, with: ::Entities::V1::Project
        end

        route_param :id, documentation: { param_type: 'string' } do
          helpers do
            def project_update_service
              @project_update_service ||= Projects::ProjectUpdateService.new(resource_params, project)
            end

            def project
              @project ||= Project.find(params[:id])
            end
          end
          desc 'Show a project by id',
               success: ::Entities::V1::Project
          get '/' do
            authorize project, :show?

            present :data, project, with: ::Entities::V1::Project
          end

          desc 'Update a project',
               success: ::Entities::V1::Project

          params do
            optional :name, type: String,
                            desc: 'Project Name',
                            documentation: { param_type: 'body' },
                            allow_blank: false
            optional :assignee_id, type: String,
                                   desc: 'Assignee_id providing null value will unassign the project',
                                   documentation: { param_type: 'body' },
                                   allow_blank: true
            optional :state, type: String,
                             desc: 'Project state',
                             documentation: { param_type: 'body' },
                             values: Settings.projects.state,
                             allow_blank: true

            optional :progress, type: Float,
                                desc: 'Project progress',
                                documentation: { param_type: 'body' },
                                allow_blank: true
          end
          patch '/' do
            authorize project, :update?

            project_update_service.perform
            present :data, project.reload, with: ::Entities::V1::Project
          end
        end
      end
    end
  end
end
