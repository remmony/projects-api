# frozen_string_literal: true

module Api
  module V1
    class EmployeesApi < Grape::API
      helpers ::Helpers::PaginationParamsHelper
      before do
        authenticate_user!
      end
      helpers do
        def resource_params
          # include_missing (false) to remove records with nil values
          @resource_params ||= declared(params, include_missing: false)
        end

        def repo
          @repo ||= ::Employees::EmployeesRepo.new(resource_params)
        end
      end
      namespace :employees do
        desc 'List all employees', success: ::Entities::V1::Employee

        params do
          use :pagination,
              sort_by_values: %w[created_at full_name role], default_sort_by_value: 'created_at'
          optional :role, type: String,
                          desc: 'filter by employee role',
                          values: %w[manager participant],
                          allow_blank: true
        end
        get '/' do
          authorize Employee, :index?
          results = repo.list_employees
          present :data, results.data, with: ::Entities::V1::Employee
          present :meta, results.meta, with: ::Entities::V1::MetaData
        end

        route_param :id, documentation: { param_type: 'string' } do
          helpers do
            def employee_update_service
              @employee_update_service ||= Employees::EmployeeUpdateService.new(resource_params, Employee)
            end

            def employee
              @employee ||= Employee.find(params[:id])
            end
          end

          desc 'Get an employee by ID',
               success: ::Entities::V1::Employee

          get '/' do
            authorize employee, :show?

            present :data, employee, with: ::Entities::V1::Employee
          end

          desc 'Delete an employee by ID', status: 204

          delete '/' do
            authorize employee, :destroy?
            employee.destroy
            status 204
          end
        end
      end
    end
  end
end
