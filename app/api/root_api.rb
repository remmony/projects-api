# frozen_string_literal: true

require 'grape-swagger'
class RootApi < Grape::API
  include ErrorsHandler
  prefix :api
  cascade true
  default_format :json
  format :json

  helpers ::Helpers::AuthenticationHelper
  helpers Pundit
  mount Api::V1Base
  add_swagger_documentation security_definitions: {
                              bearerAuth: {
                                type: 'apiKey',
                                in: 'header',
                                name: 'Authorization',
                                description: 'Token for accessing the api',
                                scheme: 'Bearer'
                              }
                            },
                            security: [{ bearerAuth: [] }],
                            hide_documentation_path: true,
                            api_version: 'v1', array_use_braces: true
end
