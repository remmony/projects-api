# frozen_string_literal: true

module Entities
  module V1
    class MetaData < Grape::Entity
      expose :total_pages, documentation: { type: 'Integer' }
      expose :current_page, documentation: { type: 'Integer' }
      expose :next_page, documentation: { type: 'Integer' }
      expose :prev_page, documentation: { type: 'Integer' }
      expose :per_page, documentation: { type: 'Integer' }
      expose :sort_by, documentation: { type: 'String' }
      expose :sort_order, documentation: { type: 'String' }
    end
  end
end
