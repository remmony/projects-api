# frozen_string_literal: true

module Entities
  module V1
    class EmployeeLite < Grape::Entity
      expose :id, documentation: { type: 'string' }
      expose :resource_type, documentation: { type: 'string' } do |_|
        'Employee'
      end
      expose :full_name, documentation: { type: 'string' }
    end

    class Owner < EmployeeLite
    end

    class Assignee < EmployeeLite
    end

    class Project < Grape::Entity
      expose :id, documentation: { type: 'string' }
      expose :resource_type, documentation: { type: 'string' } do |_|
        'Project'
      end
      expose :name, documentation: { type: 'string' }
      expose :state, documentation: { type: 'string' }
      expose :progress, documentation: { type: 'Float' }
      expose :created_at, documentation: { type: 'DateTime' } do |record|
        record.created_at.iso8601
      end
      expose :owner, documentation: { type: 'Object' }, with: Entities::V1::Owner

      expose :assignee, documentation: { type: 'Object' }, with: Entities::V1::Assignee
    end
  end
end
