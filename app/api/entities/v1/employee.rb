# frozen_string_literal: true

module Entities
  module V1
    class Employee < Grape::Entity
      expose :id, documentation: { type: 'string' }
      expose :resource_type, documentation: { type: 'string' } do |_|
        'Employee'
      end
      expose :full_name, documentation: { type: 'string' }

      expose :role, documentation: { type: 'String' }
      expose :department, documentation: { type: 'String' }
    end
  end
end
