# frozen_string_literal: true

module Errors
  class ProjectCreationError < StandardError
  end

  class ProjectUpdateError < StandardError
  end

  class InvalidAssigneeError < StandardError
  end

  class AssigneeNotInDepartmentError < StandardError
  end
end
