# == Schema Information
#
# Table name: projects
#
#  id          :uuid             not null, primary key
#  name        :string           not null
#  progress    :float            default(0.0), not null
#  state       :integer          default("planned"), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  assignee_id :uuid
#  owner_id    :uuid             not null
#
# Indexes
#
#  index_projects_on_assignee_id  (assignee_id)
#  index_projects_on_owner_id     (owner_id)
#
# Foreign Keys
#
#  fk_rails_...  (assignee_id => employees.id)
#  fk_rails_...  (owner_id => employees.id)
#
class Project < ApplicationRecord
  enum state: { planned: 0, active: 1, done: 2, failed: 3 }
  belongs_to :owner, class_name: 'Employee'
  belongs_to :assignee, class_name: 'Employee', optional: true
  delegate :full_name, to: :owner, prefix: true
  validates_uniqueness_of :name

  validates :progress, inclusion: { in: (0..100), message: 'Invalid Progress Value' }
end
