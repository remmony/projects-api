# frozen_string_literal: true

# == Schema Information
#
# Table name: employees
#
#  id                     :uuid             not null, primary key
#  authentication_token   :string
#  department             :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  full_name              :string           not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :integer          default("participant"), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_employees_on_authentication_token  (authentication_token) UNIQUE
#  index_employees_on_department            (department)
#  index_employees_on_email                 (email) UNIQUE
#  index_employees_on_reset_password_token  (reset_password_token) UNIQUE
#
class Employee < ApplicationRecord
  enum role: { participant: 0, manager: 1 }
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :owned_projects, dependent: :restrict_with_exception,
                            foreign_key: :owner_id, class_name: 'Project'
  has_many :assigned_projects, dependent: :restrict_with_exception,
                               foreign_key: :assignee_id, class_name: 'Project'
  before_save :ensure_authentication_token

  def ensure_authentication_token
    self.authentication_token ||= generate_authentication_token
  end

  private

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless Employee.where(authentication_token: token).first
    end
  end
end
