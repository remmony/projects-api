# frozen_string_literal: true

module Decorators
  module PaginationDecorator
    def prepare_results(results)
      OpenStruct.new({ data: results,
                       meta: prepare_meta(results) })
    end

    def prepare_meta(results)
      { total_pages: results.total_pages,
        current_page: results.current_page,
        next_page: results.next_page,
        prev_page: results.prev_page,
        sort_by:,
        sort_order:,
        per_page: }
    end

    def api_params
      params.symbolize_keys
    end

    def per_page
      api_params[:per_page] || Settings.default_per_page
    end

    def page
      api_params[:page] || Settings.default_page
    end

    def sort_params
      { sort_by => sort_order }
    end

    def sort_by
      api_params[:sort_by] || Settings.default_sort_by
    end

    def sort_order
      api_params[:sort_order] || Settings.default_sort_order
    end
  end
end
