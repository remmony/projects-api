# frozen_string_literal: true

module Projects
  class ProjectCreationService
    def initialize(params, employee)
      @params = params
      @employee = employee
    end

    def perform
      create_project
    rescue ActiveRecord::RecordInvalid => e
      raise Errors::ProjectCreationError, e
    end

    private

    attr_reader :params, :employee

    def create_project
      employee.owned_projects.create!(api_params)
    end

    def api_params
      params.symbolize_keys
    end
  end
end
