# frozen_string_literal: true

module Projects
  class ProjectUpdateService
    def initialize(params, project)
      @project = project
      @params = params
    end

    def perform
      validate_assignee!

      project.update!(api_params)
    rescue ActiveRecord::RecordInvalid => e
      raise Errors::ProjectUpdateError, e
    end

    private

    attr_reader :project, :params

    def assignee
      Employee.find(api_params[:assignee_id])
    rescue ActiveRecord::RecordNotFound => e
      raise Errors::InvalidAssigneeError
    end

    def validate_assignee!
      return unless api_params[:assignee_id]

      raise Errors::AssigneeNotInDepartmentError if project.owner.department != assignee.department
    end

    def api_params
      params.symbolize_keys
    end
  end
end
