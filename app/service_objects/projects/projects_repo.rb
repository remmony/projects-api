# frozen_string_literal: true

module Projects
  class ProjectsRepo
    include Decorators::PaginationDecorator
    def initialize(params)
      @params = params
    end

    def list_projects
      results = Project.where(where_clause)
                       .order(sort_params).page(page).per(per_page)
      prepare_results(results)
    end

    private

    attr_reader :params

    def where_clause
      return [] unless api_params[:state].present?

      { state: api_params[:state] }
    end
  end
end
