# frozen_string_literal: true

module Employees
  class EmployeesRepo
    include Decorators::PaginationDecorator
    def initialize(params)
      @params = params
    end

    def list_employees
      results = Employee
                .where(where_clause)
                .order(sort_params).page(page).per(per_page)
      prepare_results(results)
    end

    private

    attr_reader :params

    def where_clause
      return [] unless api_params[:role].present?

      { role: api_params[:role] }
    end
  end
end
