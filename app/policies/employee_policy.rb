# frozen_string_literal: true

class EmployeePolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def update?
    create? && !record.manager?
  end

  def destroy?
    update?
  end

  def create?
    user.manager?
  end
end
