# frozen_string_literal: true

class ProjectPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    index?
  end

  def update?
    create?
  end

  def create?
    user.manager?
  end
end
