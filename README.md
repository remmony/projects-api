## Projects Api

### A Ruby on Rails Application for creatings project and tasks

- Ruby version : 3.1.3

- Rails Version 7.0.4

### About the application

The app consists of employees and projects , employees can be either `managers` or `participants`

- `managers` can create projects and update projects
- I could make a specific endpoint for assigning projects but in real applications in `Jira` for example , you can update the project and also assigning/unassigning a user on the same time
- Projects must have a unique name .
- some additional endpoints like deleting an employee are provided , managers can delete participants only , in case participant has associated projects , you must delete the projects first or change the owner/assignee .

### Run locally

- Ensure that you have `Docker` and `docker-compose` installed in your machine
- Clone the Repo
- Cd inside the root directory
- Run `docker-compose up` to setup the application and to run some seeds for `employees` and `projects`
- Go to `localhost:3000/api-docs/` click on `Authorize` on the top right then add `123456` then try the endpoints there
- To login inside the container use `docker-compose exec web bash`
- Inside the container run `rails c` .

#### run tests locally

- run `docker-compose exec web bash -c 'bundle exec rspec'` command
