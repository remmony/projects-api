class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects, id: :uuid do |t|
      t.string :name, null: false
      t.integer :state, null: false, default: 0
      t.float :progress, null: false, default: 0
      t.references :owner, foreign_key: { to_table: :employees }, index: true,
                           null: false, type: :uuid

      t.references :assignee, foreign_key: { to_table: :employees }, index: true,
                              type: :uuid
      t.timestamps
    end
  end
end
