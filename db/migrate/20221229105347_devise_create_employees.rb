# frozen_string_literal: true

class DeviseCreateEmployees < ActiveRecord::Migration[7.0]
  def change
    create_table :employees, id: :uuid do |t|
      t.string :email, null: false, default: ''
      t.string :encrypted_password, null: false, default: ''

      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      t.datetime :remember_created_at

      t.string :authentication_token
      t.timestamps null: false
      t.string :full_name, null: false
      t.integer :role, null: false, default: 0
      t.string :department, index: true
    end

    add_index :employees, :email,                unique: true
    add_index :employees, :reset_password_token, unique: true
    add_index :employees, :authentication_token, unique: true
  end
end
